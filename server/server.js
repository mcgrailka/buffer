const express = require('express');
const serve = require('serve-static');
const morgan = require('morgan');
const path = require('path');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const {
  getDayStartFromUnixTimestamp,
  getTimestampSeriesArray,
} = require('./lib/dates');
const {
  mapStatistics,
} = require('./lib/utility');

const app = express();
const PORT = 8080;

const adapter = new FileSync(path.join(__dirname, './database/db.json'));
const db = low(adapter);

// Logger
app.use(morgan('dev'));

// Disable caching for API endpoints
app.use('/api', (req, res, next) => {
  res.header('Cache-Control', 'no-cache');
  next();
});

app.get('/api/getAnalyticsTimeseries', (req, res) => {
  let updatesAnalytics = db.get('updates-analytics').value();

  let updates = db.get('updates')
    .map(mapStatistics(updatesAnalytics))
    .map(analytic => {
      return {
        ...analytic,
        sent_at_day_timestamp: getDayStartFromUnixTimestamp(analytic.sent_at)
      };
    })
    .orderBy('sent_at', 'desc');

  let firstUpdateAnalytic = updates.head().value().sent_at_day_timestamp;
  let lastUpdateAnalytic = updates.last().value().sent_at_day_timestamp;

  let timeSeriesRange = {};
  let timestampSeries = getTimestampSeriesArray(Math.min(firstUpdateAnalytic, lastUpdateAnalytic), Math.max(firstUpdateAnalytic, lastUpdateAnalytic));

  timestampSeries
    .forEach(timestamp => {
      timeSeriesRange[timestamp] = {
        timestamp,
        retweets: 0,
        favorites: 0,
        clicks: 0
      }
    });

  let resultArray = updates
    .reduce((accumulated, update) => {
      let accumulator = { ...accumulated };

      let updatesTimeSeriesEntry = accumulator[update.sent_at_day_timestamp];

      if (!updatesTimeSeriesEntry) {
        console.log(`Error: Timeseries does not exist for present entry (${update.id})`);
        return accumulator;
      }

      let {retweets, favorites, clicks } = update.statistics;
      let {
        retweets: acc_retweets,
        favorites: acc_favorites,
        clicks: acc_clicks
      } = updatesTimeSeriesEntry;

      accumulator[update.sent_at_day_timestamp] = {
        ...updatesTimeSeriesEntry,
        retweets: retweets + acc_retweets,
        favorites: favorites + acc_favorites,
        clicks: clicks + acc_clicks,
      };

      return accumulator;

    }, timeSeriesRange).value();

  res.json(Object.values(resultArray));
});

app.get('/api/getUpdates', (req, res) => {
  const pageSize = 10;
  const defaultParams = {page: 1};
  const {page} = {...defaultParams, ...req.query};
  const totalRecords = db.get('updates').size().value();

  const parsedPage = parseInt(page);
  let currentPage = typeof parsedPage === 'number' && parsedPage > 0 ? parsedPage : 1;

  const pageStart = (currentPage - 1) * pageSize;
  const pageEnd = pageStart + pageSize;

  const maxPage = Math.ceil(totalRecords / pageSize);
  let updates = [];

  if (currentPage <= maxPage) {
    let updatesAnalytics = db.get('updates-analytics').value();
    updates = db.get('updates')
      .orderBy('sent_at', 'desc')
      .slice(pageStart, pageEnd)
      .map(mapStatistics(updatesAnalytics))
      .value();
  } else {
    currentPage = maxPage;
  }

  const nextPage = currentPage + 1 > maxPage ? maxPage : currentPage + 1;
  const prevPage = currentPage - 1 <= 0 ? 1 : currentPage - 1;

  res.json({
    updates,
    updatesInfo: {
      totalRecords,
      currentPage,
      nextPage,
      prevPage,
      maxPage
    },
    updatesLoadedAll: nextPage <= currentPage
  });
});

// Serve static assets in the /public directory
app.use(serve(path.join(__dirname, '../public'), {
  cacheControl: 'no-cache'
}));

app.use((err, req, res, next) => {
  // Handle missing file in public dir as a 404
  if (err.code === 'ENOENT') {
    return res.status(404).send('404 - Page not found');
  }
  console.log(err);
  res.status(500).send(err);
});

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
