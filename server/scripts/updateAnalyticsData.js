const path = require('path');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const fetch = require('node-fetch');

const adapter = new FileSync(path.join(__dirname, '../database/db.json'));
const db = low(adapter);

const {
  mapStatistics,
} = require('../lib/utility');

const getTweetsUrl = (tweetIds = []) => `http://code-exercise-api.buffer.com/getTweets?ids=${tweetIds.join(',')}`;

const updatesAnalyticsCollection = db.get('updates-analytics');
const updatesCollection = db.get('updates');

const updatesAnalytics = updatesAnalyticsCollection.value();

const updatesWithAnalytics = updatesCollection
  .map(mapStatistics(updatesAnalytics))
  .value();

const tweetUrl = getTweetsUrl(updatesWithAnalytics.map(update => update.service_update_id));

console.log(`Retrieving updated Analytics data`);
fetch(tweetUrl)
  .then(response => {
    if (!response.ok) {
      return [];
    }
    return response.json();
  }).then(updatedAnalytics => {
    console.log(`Records retrieved (total: ${updatedAnalytics.length})`);
    return updatedAnalytics.map(analytics => {
      let {
        id,
        retweet_count: retweets,
        favorite_count: favorites,
        click_count: clicks
      } = analytics;

      return {
        id,
        retweets,
        favorites,
        clicks
      };
    });
}).then(resolvedAnalytics => {
  console.log(`Records mapped for update / insertion`);
  const analyticsInsertionResult = resolvedAnalytics.reduce((accumulated, analyticRecord) => {
    let accumulator = { ...accumulated };
    const {id, ...analytic} = analyticRecord;
    let updateTweet = updatesWithAnalytics.find(update => update.service_update_id === id);

    if (!updateTweet) {
      console.log(`Tweet with id: ${id} does not exist.`);
      accumulator.missing = [accumulator.missing, analyticRecord];
      return accumulator;
    }
    if (updateTweet.statistics) {
      let updatedRecord = updatesAnalyticsCollection
        .find({update_id: updateTweet.statistics.update_id})
        .assign(analytic)
        .write();

      accumulator.updated = [ ...accumulator.updated, updatedRecord];
      return accumulator;
    }

    let insertedRecord = updatesAnalyticsCollection
      .insert({
        update_id: updateTweet.id,
        ...analytic
      })
      .write;

    accumulator.inserted = [ ...accumulator.inserted, insertedRecord ];
    return accumulator;
  }, { inserted: [], updated: [], missing: []});

  let insertedTotal = analyticsInsertionResult.inserted.length;
  let updatedTotal = analyticsInsertionResult.updated.length;
  let missingTotal = analyticsInsertionResult.missing.length;

  console.log(`Records updated (inserted: ${insertedTotal}, updated: ${updatedTotal}, missing: ${missingTotal})`);
}).catch(error => {
  console.log(`Error Updating records`);
  console.error(error);
});


