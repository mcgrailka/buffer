/**
 * mapStatistics
 *
 * Given a lowdb updates-analytics collection, return a new object with a statistics object if one exists
 * for each individual update element
 */
module.exports.mapStatistics = updatesAnalytics => update => {
  const statistics = updatesAnalytics.find(analytic => analytic.update_id === update.id);
  if (!statistics) {
    return update;
  }
  return {...update, statistics};
};
