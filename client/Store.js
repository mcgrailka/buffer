import Api from './Api';
import actions from './actions';

window.Api = Api;

class Store {

  constructor(initialState = {}) {
    this.state = initialState;
    this.onChangeCallbacks = [];
  }

  getState(key) {
    if (key) {
      return this.state[key];
    }
    return this.state;
  }

  setState(key, value) {
    if(typeof key === 'object') {
      for (let item in key ) {
        if (key.hasOwnProperty(item)) {
          this.state[item] = key[item];
        }
      }
    } else {
      this.state[key] = value;
    }
    this.emitChange();
  }

  onChange(callback) {
    this.onChangeCallbacks.push(callback);
  }

  emitChange() {
    this.onChangeCallbacks.forEach(callback => {
        callback();
    });
  }

  handleError(err) {
    console.error(err);
  }

  dispatch(action) {
    switch (action.type) {
      case actions.LOAD_UPDATES:
        let page = action.page || 1;
        let currentUpdates = this.getState('updates');
        let currentInfo = this.getState('updatesInfo');
        Api.get('getUpdates', { page })
          .then(({ updates, updatesInfo, updatesLoadedAll }) => {
            let resolvedUpdates = updates;
            let resolvedUpdatesInfo = updatesInfo;
            let resolvedUpdatesLoadedAll = updatesLoadedAll;
            if(currentInfo) {
              if (currentInfo.currentPage < updatesInfo.currentPage ) {
                resolvedUpdates = [ ...currentUpdates, ...resolvedUpdates ];
              } else {
                resolvedUpdates = currentUpdates;
              }
            }
            this.setState({
              updates: resolvedUpdates,
              updatesInfo: resolvedUpdatesInfo,
              updatesLoadedAll: resolvedUpdatesLoadedAll
            });
          })
          .catch(this.handleError);
        break;
      case actions.LOAD_ANALYTICS:
        Api.get('getAnalyticsTimeseries')
          .then(timeseries => {
            this.setState('analyticsTimeseries', timeseries);
          })
          .catch(this.handleError);
        break;
    }
  }

}

export default Store;
