import React, { Component } from 'react';
import actions from  '../actions';
import Update from  './Update';

class UpdateList extends Component {

  constructor(props) {
    super(props);
    this.loadPage = this.loadPage.bind(this);
  }

  loadPage() {
    let {
        dispatch,
        updatesInfo: {nextPage}
    } = this.props;

    dispatch({
      type: actions.LOAD_UPDATES,
      page: nextPage
    });
  };

  render() {
    if (!this.props.updates || !this.props.updates.length) {
      return <p>You have no updates</p>
    }

    return (
      <div className="update-list">
        {
          this.props.updates.map((update, idx) => (
            <Update
              {...update}
              key={idx}
            />
          ))
        }
        {
          !this.props.updatesLoadedAll
            ? <div className="update-list-load-more">
                <button onClick={this.loadPage} >Load More</button>
              </div>
            : null
        }
      </div>
    );
  }
}

export default UpdateList;
