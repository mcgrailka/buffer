import React, { Component } from 'react';
import UpdateList from './UpdateList';
import AnalyticsChart from './AnalyticsChart';
import AnalyticsSummary from './AnalyticsSummary';

class App extends Component {

  render() {
    let {
      updates,
      info,
      dispatch,
      updatesLoadedAll,
      updatesInfo,
      analyticsTimeseries
    } = this.props;

    return (
      <div className="app">
        <img
          src="/logo-buffer.svg"
          alt="Buffer"
          className="logo"
        />
        <h2>Analytics</h2>
        <AnalyticsChart
          analyticsTimeseries={analyticsTimeseries}
        />
        <h2>Recent posts</h2>
        <div className="main-container">
          <UpdateList
            updatesLoadedAll={updatesLoadedAll}
            updates={updates}
            updatesInfo={updatesInfo}
            dispatch={dispatch}
          />
          <AnalyticsSummary
            updates={updates}
          />
        </div>
      </div>
    );
  }
}

export default App;
